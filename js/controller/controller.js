function renderDanhSachSP(qlspArr) {
  console.log("🚀 ~ file: controller.js:2 ~ renderDanhSachSP ~ qlspArr", qlspArr)
  var contentHTML = "";

  qlspArr.forEach(function (DS) {
    var content = `
        <div class="item" data-key="1">
        <div class="img">
            <img src="${DS.img}" alt="">
        </div>
        <div class="content">
            <div class="title">${DS.name}</div>
            <div class="des">
                ${DS.desc}
            </div>
            <div class="price">${DS.price}</div>
            <input type="number" class="count" min="1" value="1">
            <button onclick="themSP()" class="add">Add to cart</button>
            <button  onclick="xoaSP(1)"  class="remove"><i class="fa-solid fa-trash-can"></i></button>
        </div>
    </div>
    
        `;

    contentHTML += content;
  });

  document.getElementById('list').innerHTML = contentHTML;
}
