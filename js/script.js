const BASE_URL = "https://63d93875baa0f79e09b795eb.mockapi.io";
var DS = [];
var filteredProductList = [];


function fetchQLSPList() {
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then(function (res) {
      renderDanhSachSP(res.data);
      DS.push(...res.data);
    })

    .catch(function (err) {
      console.log("🚀 ~ file: script.js:15 ~ fetchQLSPList ~ err", err);
    });
}

fetchQLSPList();

function timKiem() {
  let search = document.getElementById("textsearch").value;
  let spSearch = DS.filter(function (value) {
    return value.name.toUpperCase().includes(search.toUpperCase());
  });
  renderDanhSachSP(spSearch);
}

document.getElementById('productTypeSelect').addEventListener('change', function() {
  var selectedType = this.value;

  filteredProductList = DS.filter(function(product) {
    return product.type === selectedType;
  });
    
  if(filteredProductList.length === 0){
    return renderDanhSachSP(DS);
    }
   
  renderDanhSachSP(filteredProductList);
});

